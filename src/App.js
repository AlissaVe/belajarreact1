
import Header from './components/Header';
import CardWrapper from './components/CardWrapper';
import CardCategory from './components/CardCategory';
import Footer from './components/Footer';
import './styles/global.css';
import CardList from './components/CardList';
import CardItem from './components/CardItem';

import hukum from "./image/kinerja.png";

function App() {
  return (

    <div className="mobile-container">
      {/* <div className="App"> */}
      <Header name="alissa1" />
      <main>
        <div className="main-container">
          <CardWrapper title="Categori">


            <CardList>
              <CardItem img={hukum} name="Designer"> </CardItem>
              <CardItem img={hukum} name="Administrator"> </CardItem>

            </CardList>

          </CardWrapper>
          <CardWrapper title="Top 50 Jobs">

            <CardList >
              <CardItem img={hukum} name="Designer"> </CardItem>
              <CardItem img={hukum} name="Administrator"> </CardItem>


            </CardList>

          </CardWrapper>


        </div>
      </main>
      <Footer />
      {/* </div> */}
    </div>
  );
}

export default App;
