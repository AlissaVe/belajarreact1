import "./CardStyles.css";

export default function CardItem(props) {
    const { img, name } = props;
    return (
        //hanya bisa ada satu div (parent)
        <div className="flexbox-item">
            <div className="flexbox-inner">
                <figure className="img-item">
                    <img src={img} alt="card-item-img" width="50px" />
                </figure>

                <div className="content">{name}</div>

            </div>
        </div>
    )
}
