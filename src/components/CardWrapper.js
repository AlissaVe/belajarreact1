import "./CardStyles.css";

export default function CardWrapper(props) {

    return (
        //hanya bisa ada satu div (parent)
        <div className="m-wrapper">
            <p className="title">
                {props.title}
            </p>
            <br/>
            
            {props.children}
        </div>
    )
}