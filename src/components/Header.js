import "./Header.css";

export default function Header(props) {

    return (
        //hanya bisa ada satu div (parent)
        <div className="m-header">
            <div className="m-header__btn">
                <svg width="24" height="24" fill="currentColor" class="bi bi-arrow-left" viewBox="0 0 16 16">
                    <path fill-rule="evenodd" d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z" />
                </svg>
            </div>

            <div className="m-header__form">
            <input type="text" class="input-control" placeholder="search"/>
            </div>
            {/* <h2>{props.name}</h2> */}
            {/* <button>Header</button> */}
        </div>
    )
}